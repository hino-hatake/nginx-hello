# NGINX sends their regards
`nginx` webserver that serves a simple page containing its hostname, IP addr, client IP addr as wells as the request URI and the local time of the webserver.

Sauce: https://github.com/nginxinc/NGINX-Demos

The images are uploaded to Docker Hub:

~~https://hub.docker.com/r/nginxdemos/hello/~~

https://hub.docker.com/r/hinorashi/nginx-hello-proxy

How to run:
```bash
docker run -p 80:80 -p 8080:8080 -d hinorashi/nginx-hello-proxy
docker-compose up -d 
```

Now, in a browser we can make a request to the webserver and get the page below (actually I changed the logo :whale:):
![hello](hello.png)

A plain text version of the image is available as `hinorashi/nginx-hello-proxy:plain-text`.
You can update the `.env` file to change the docker compose file in action.

This version returns the same information in the plain text format:
```bash
curl <ip>:<port>
curl localhost
```
For PROXY protocol:
```bash
curl --haproxy-protocol <ip>:<proxy-port>
curl --haproxy-protocol localhost:8080
```
Output should be quite the same:
```
Server address: 172.17.0.2:8x
Server name: 22becba5323d
Date: 07/Feb/2018:16:05:05 +0000
URI: /
Request ID: 48ba0db334a6ed165e783469c2af868f
```

The images were created to be used as simple backends for various load balancing demos.
