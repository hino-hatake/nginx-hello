# syntax=docker/dockerfile:1
FROM nginx:mainline-alpine
RUN rm /etc/nginx/conf.d/*
ADD hello.proxy.conf /etc/nginx/conf.d/
ADD index.html /usr/share/nginx/html/
EXPOSE 80

